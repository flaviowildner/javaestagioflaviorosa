package candidatura.questao8;

public class NaoChecadaException extends RuntimeException{
	
	public NaoChecadaException(){}
	
	public NaoChecadaException(String mensagem){
		super(mensagem);
	}
}
