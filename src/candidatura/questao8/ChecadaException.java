package candidatura.questao8;

public class ChecadaException extends Exception{

	public ChecadaException(){}
	
	public ChecadaException(String mensagem){
		super(mensagem);
	}
}