package candidatura.questao8;

import java.time.LocalDateTime;

public class Excecao {
	
	static public void lancaChecada() throws ChecadaException{
		LocalDateTime now = LocalDateTime.now();
		if(now.getDayOfMonth() % 2 != 0){
			throw new ChecadaException("Erro");
		}
	}
	
	static public void propagaChecada() throws ChecadaException{
		lancaChecada();
	}
	
	static public void trataChecada(){
		try {
			propagaChecada();
		} catch (ChecadaException e) {
			//System.out.println(e.getMessage());	
		}finally{
			System.out.println("Tratamento de exceção");
		}	
	}
}
