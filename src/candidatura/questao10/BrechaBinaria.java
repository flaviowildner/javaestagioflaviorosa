package candidatura.questao10;

public class BrechaBinaria {
	public int solucao(int N){
		int state = 0;
		int higher = 0;
		int count = 0;
		
		
		if(N < 1 || N > 2147483647)
			return 0;
		
		System.out.println(Integer.toBinaryString(N)); //Print o número N em formato binário
		
		
		for(int i=0;i<Integer.SIZE - 1;i++){
			if((N & 1) == 1){
				state = 1;
				if(count > higher){
					higher = count;
					count = 0;
				}
			}
			if((N & 1) == 0 && state == 1){ //
				count++;
			}
			N = N >> 1;
		}
		if(higher != 0)
			return higher;
		else
			return 0;
	}
}
