package candidatura.questao6;

public abstract class ClasseAbstrata {
	static int valorParaTodas;
	private final int constante = 5; //Constante nunca será acessada, a classe Abstrata não pode ser instanciada para acessar a constante.
	
	public abstract void meExtenda();
}