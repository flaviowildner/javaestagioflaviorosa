/*Provavelmente há um problema no enunciado da questão,
 * pois o dobro de qualquer número é par, e todo número par tem o divisor 2.
 * Sendo assim para qualquer combinação de R e S, a execução irá para em 2.
 * 
 * Por exemplo:
 * Para R = 3 e S = 15
 * Número pares de 1 a S: 2 4 6 8 10 12 14
 * Dobro de R é 6, e 2 é divisor de 6, então a execução é interrompida no 2.
 * 
 * Ao parar a execução o enunciado pede para imprimir a mensagem "Múltiplo do dobro de R encontrado",
 * o que não faz sentido, pois 2 não é múltiplo de 6.
 * 
 * O algoritmo abaixo está na forma como o enunciado pede mesmo não fazendo sentido.
 */

package candidatura.questao2;


import java.util.Random;


public class Main {

	public static void main(String[] args) {
		
		Random randomNumber = new Random();
		
		int R = randomNumber.nextInt(10) + 1;
		int S = randomNumber.nextInt(100) + 1;
		
		System.out.println("Number R = " + R + ":");
		System.out.println("Number S = " + S + ":\n");
		
		for(int i=1;i<=S;i++){
			if(i % 2 == 0){
				if((2 * R % i) == 0){
					System.out.println("Múltiplo do dobro de R encontrado");
					break;
				}else{
					System.out.println(i);
				}
			}
		}
	}
}
