package candidatura.questao9;


public class Main {
	public static void main(String[] args) throws CloneNotSupportedException {
		
		//1
		System.out.println("9.1:");
		Pessoa.criarPessoas("João", 10);
		Pessoa.criarPessoas("Alice", 5);
		Pessoa.criarPessoas("Fernando", 27);
		Pessoa.criarPessoas("Carlos", 12);
		Pessoa.criarPessoas("Priscila", 31);
		
		Pessoa.imprimirOrdenadoIdade();
		
		
		//2
		System.out.println("\n9.2:");
		Pessoa.removePelaIdade(27);
		
		
		
		//3
		System.out.println("\n9.3:");
		Pessoa.resetClass();//Resetar todos os atributos estáticos
		Pessoa.criarPessoas("Paulo", 8);
		Pessoa.criarPessoas("Silas", 19);
		Pessoa.criarPessoas("Paulo", 18);
		Pessoa.criarPessoas("Pedro", 25);
		Pessoa.criarPessoas("Paulo", 50);
		Pessoa.printDistintoNome();
		
		
		
		//4
		System.out.println("\n9.4:");
		Pessoa.resetClass();
		Pessoa.criarPessoas("Paulo", 8);
		Pessoa.criarPessoas("Silas", 19);
		Pessoa.criarPessoas("Paulo", 18);
		Pessoa.criarPessoas("Pedro", 25);
		Pessoa.criarPessoas("Paulo", 50);
		
		CachePessoa.retornaPessoa(5);
		CachePessoa.retornaPessoa(4);
		CachePessoa.retornaPessoa(2);
		CachePessoa.retornaPessoa(1);
		CachePessoa.retornaPessoa(5);
		CachePessoa.retornaPessoa(1);
		CachePessoa.retornaPessoa(3);
		CachePessoa.retornaPessoa(4);
		CachePessoa.retornaPessoa(2);
		

		
	}
}
