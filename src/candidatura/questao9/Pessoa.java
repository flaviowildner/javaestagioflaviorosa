package candidatura.questao9;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Pessoa implements Cloneable{
	static List<Pessoa> listaDePessoas = new ArrayList<Pessoa>();
	static int nPessoas = 0;
	
	public int id;
	public String nome;
	public int idade;
	
	public Pessoa(String nome, int idade) {
		nPessoas++; /*Como nenhuma Pessoa terá um id maior que as pessoas criadas anteriormente, a lista de
							pessoas sempre será ordenada, permitindo assim o uso da busca Binária.*/
		this.id = nPessoas;
		this.nome = nome;
		this.idade = idade;
	}
	
	public static void criarPessoas(String nome, int idade){
		listaDePessoas.add(new Pessoa(nome, idade));
	}
	
	public static void imprimirOrdenadoIdade() throws CloneNotSupportedException{
		List<Pessoa> tempListaDePessoas = new ArrayList<Pessoa>();
		
		for(int i=0;i<Pessoa.listaDePessoas.size();i++){
			tempListaDePessoas.add(Pessoa.listaDePessoas.get(i).clone());
		}
		
		Collections.sort(tempListaDePessoas, (Pessoa p1, Pessoa p2) -> p1.idade-p2.idade);
		
		
		for(int i=0;i<Pessoa.listaDePessoas.size();i++){
			System.out.println(tempListaDePessoas.get(i).nome + " " + tempListaDePessoas.get(i).idade);
		}
	}
	
	public static void removePelaIdade(int idade){
		System.out.println("Antes:");
		for(int i=0;i<Pessoa.listaDePessoas.size();i++){
			System.out.println(Pessoa.listaDePessoas.get(i).nome + " " + Pessoa.listaDePessoas.get(i).idade);
		}
		
		for(int i=0;i<listaDePessoas.size();i++){
			if(listaDePessoas.get(i).idade == idade){
				listaDePessoas.remove(i);
				i = -1;
			}
		}
		
		System.out.println("Depois:");
		for(int i=0;i<Pessoa.listaDePessoas.size();i++){
			System.out.println(Pessoa.listaDePessoas.get(i).nome + " " + Pessoa.listaDePessoas.get(i).idade);
		}
	}
	
	
	
	/*
	 * Quanto maior o banco de pessoas, o algoritmo trivial tende a ficar extremamente lento, já que será necessário buscar toda a lista de nomes já encontrados.
	 * Utilizando tabela de dispersão, não é necessário percorrer toda a lista, pois o nomes estarão em buckets pela função de dispersão.
	 * 
	 */
	public static void printDistintoNome(){
		System.out.println("Lista de nomes completa:");
		for(int i=0;i<Pessoa.listaDePessoas.size();i++){
			System.out.println(Pessoa.listaDePessoas.get(i).nome);
		}
		
		Map<Integer, List<String>> hashString = new HashMap<Integer, List<String>>();


		for(int i=0;i<Pessoa.listaDePessoas.size();i++){
			int hc = Pessoa.listaDePessoas.get(i).nome.hashCode();
			if(!hashString.containsKey(hc)){
				List<String> tempList = new ArrayList<String>();
				tempList.add(Pessoa.listaDePessoas.get(i).nome);
				hashString.put(hc, tempList);
			}else{
				for(int j=0;j<hashString.get(hc).size();j++){
					if(hashString.get(hc).get(j) == Pessoa.listaDePessoas.get(i).nome){
						break;
					}else if(j == hashString.get(hc).size() - 1){
						hashString.get(hc).add(Pessoa.listaDePessoas.get(i).nome);
					}
				}
			}
		}
		
		System.out.println("Distintos:");
		
		for(Integer key : hashString.keySet()){
			for(int i=0;i<hashString.get(key).size();i++){
				System.out.println(hashString.get(key).get(i));
			}
		}	
		
	}
	
	
	public Pessoa clone() throws CloneNotSupportedException{
		return (Pessoa)super.clone();
	}
	
	public static void resetClass(){
		listaDePessoas.clear();//Clear ao invés de removeAll, pois não é necessário executar um desconstrutor em Pessoa.
		nPessoas = 0;
	}

}
