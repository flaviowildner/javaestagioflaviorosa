package candidatura.questao9;

import java.util.ArrayList;
import java.util.List;

public class CachePessoa {
	
	public static List<Pessoa> cacheListaDePessoas = new ArrayList<Pessoa>();
	
	public static void retornaPessoa(int id) throws CloneNotSupportedException{
		Pessoa buscaCache = buscaBinaria(cacheListaDePessoas, id);
		if(buscaCache != null){
			System.out.println(buscaCache.id + " " + buscaCache.nome + " " + buscaCache.idade + " - " + "Cache!");
		}else{
			Pessoa tempPessoa = buscaBinaria(Pessoa.listaDePessoas, id);
			if(tempPessoa != null){
				int i = 0;
				do{
					if(cacheListaDePessoas.size() == 0){
						cacheListaDePessoas.add(tempPessoa.clone());
						break;
					}else if(tempPessoa.id < cacheListaDePessoas.get(i).id){
						cacheListaDePessoas.add(i, tempPessoa.clone());
						break;
					}else if(i >= cacheListaDePessoas.size() - 1){
						cacheListaDePessoas.add(tempPessoa.clone());
						break;
					}
					i++;
				}while(i < cacheListaDePessoas.size());
				System.out.println(tempPessoa.id + " " + tempPessoa.nome + " " + tempPessoa.idade + " - " + "Lista!");
			}else{
				System.out.println("Não existe uma pessoa com o id " + id + ".");
			}
		}
	}
	
	
	public static Pessoa buscaBinaria(List<Pessoa> listaDePessoas, int id){
		int inf = 0;
		int sup = listaDePessoas.size() - 1;
		int meio;
		
		while(inf <= sup){
			meio = (inf + sup)/2;
			if (id == listaDePessoas.get(meio).id)
				return listaDePessoas.get(meio);
			if (id < listaDePessoas.get(meio).id)
				sup = meio - 1;
			else
				inf = meio + 1;
		}
		return null;
	}
}
