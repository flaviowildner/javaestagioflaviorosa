package candidatura.questao3;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		Random randomNumber = new Random();
		
		int R = randomNumber.nextInt(10) + 1;
		int S = randomNumber.nextInt(1000) + 1;
		int somatorio = 0;
		
		
		System.out.println("Number R = " + R + ":");
		System.out.println("Number S = " + S + ":\n");
		
		for(int i=1;i<=S;i++){
			if(R % i == 0){
				somatorio+=i;
				System.out.println(i);
			}
			if(i > R)//Após i ser maior que R, R não terá mais divisores, então a execução é interrompida.
				break;
		}
		System.out.println("Soma: " + somatorio);
	}
}
