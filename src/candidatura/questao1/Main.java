package candidatura.questao1;

import java.util.Random;

public class Main {

	public static void main(String[] args) {
		Random randomNumber = new Random();
		int R = randomNumber.nextInt(100) + 1;
		
		System.out.println("Numero R = " + R + ":\n");
		
		for(int i=1;i<=R;i++){
			if(i % 2 == 0 && !(i % 5 == 0)){
				System.out.println(i);
			}
		}
	}
}