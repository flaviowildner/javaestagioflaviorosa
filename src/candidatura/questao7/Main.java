package candidatura.questao7;


public class Main {

	public static void main(String[] args) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
	
		String letra = "c";
		
		executar(letra);
	}
	
	
	
	static void executar(String letra) throws InstantiationException, IllegalAccessException, ClassNotFoundException {
		Contrato temp = null;
		try{
			temp = (Contrato) Class.forName(Main.class.getPackage().getName() + "." + letra).newInstance();
		}catch(ClassNotFoundException ex){
			System.out.println("Não existe uma classe com esse nome.");
		}catch(NoClassDefFoundError ex){
			letra = letra.toUpperCase();
			temp = (Contrato) Class.forName(Main.class.getPackage().getName() + "." + letra).newInstance();
		}
		if(temp != null)
			temp.implementeMe();
	}
}