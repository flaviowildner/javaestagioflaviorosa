package candidatura.questao5;

public class Main {

	public static void main(String[] args) {
		
		Animal animal = new Animal();
		Ave ave = new Ave();
		Mamífero mamifero = new Mamífero();
		
		animal.comer();
		animal.dormir();
		ave.voar();
		mamifero.mamar();
	
	}
}
