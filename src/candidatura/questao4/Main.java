package candidatura.questao4;

import java.time.Month;
import java.util.Random;


public class Main {

	private void diasMes(Month M){
		int nDays = M.length(true);
		
		switch(nDays){
		case 31:
			System.out.println("Mês com 31 dias.");
			break;
		case 30:
			System.out.println("Mês com 30 dias.");
			break;
		default:
			System.out.println("Mês com menos de 30 dias.");
			break;
		}
	}

	final private void qualDiaMes(){
		Random randomNumber = new Random();
		int numero = randomNumber.nextInt(12) + 1;
		Month mes = Month.of(numero);
		//System.out.println(mes);//Mes
		diasMes(mes);
	}
	
	
	
	public static void main(String[] args) {
		Main main = new Main();
		
		main.qualDiaMes();
	}
}
